var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoOp = require("./models/mongo");
var router = express.Router();
var port = 8080;

//var nbrDureeSec = 1000;
//var initTimestamp = "2016-10-31T13:13:18.947Z";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    "extended": false
}));

router.get("/", function(req, res) {
    res.json({
        "error": false,
        "message": "Hello World"
    });
});

//route() will allow you to use same path for different HTTP operation.
//So if you have same URL but with different HTTP OP such as POST,GET etc
//Then use route() to remove redundant code.
router.route("/messages")
    .post(function(req, res) {

        var db = new mongoOp();
        var response = {};

        db.id = req.body.id;
        //db.timestamp = req.body.timestamp;
        db.timestamp = new Date(req.body.timestamp);
        db.sensortype = req.body.sensortype;
        db.value = req.body.value;

        db.save(function(err) {
            // save() will run insert() command of MongoDB.
            // it will add new data in collection.
            if (err) {
                response = {
                    "error": true,
                    "message": "Error adding data",
                    "errfor": err
                };
            } else {
                response = {
                    "error": false,
                    "message": "Data added"
                };
            }
            res.json(response);
        });
    });
router.route("/messages/synthesis")
    .get(function(req, res) {

        var initTimestamp = req.query.timestamp;
        var nbrDureeSec = req.query.duration;

        var t = new Date(initTimestamp);
        t.setSeconds(t.getSeconds() + nbrDureeSec);


        mongoOp.aggregate([{
            $match: {
                timestamp: {
                    $gte: new Date(initTimestamp),
                    $lt: t
                }
            }
        }, {
            $group: {
                _id: '$sensortype',
                max: {
                    $max: "$value"
                },
                min: {
                    $min: "$value"
                },
                avg: {
                    $avg: "$value"
                }
            }
        }], function(err, result) {
            if (err) {
                //                next(err);
                console.log(err);
            } else {
                res.json(result);
                //console.log(result);
            }
        });


        /*
            var response = {};
            mongoOp.find({},function(err,data){
            // Mongo command to fetch all data from collection.
                if(err) {
                    response = {"error" : true,"message" : "Error fetching data"};
                } else {
                    response = {"error" : false,"message" : data};
                }
                res.json(response);
            });
        */


    })

app.use('/', router);

app.listen(serveur);
console.log("Listening to PORT %d", port);
