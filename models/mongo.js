var mongoose    =   require("mongoose");
mongoose.connect('mongodb://localhost:27017/demoDB');
// create instance of Schema
var mongoSchema =   mongoose.Schema;
// create schema
var userSchema  = {
    id: String,
    timestamp : Date,
    sensortype : Number,
    value : Number
};
// create model if not exists.
module.exports = mongoose.model('message',userSchema);